#!/bin/bash
# reboot if ping test fails for 3 times
  
unset IVAR
  
for i in {1..3}; do
  ping -c 1 8.8.8.8 > /dev/null || ((IVAR++))
done
  
if [ "$IVAR" == 3 ]; then
  echo '[' $( date '+%d/%m/%Y %R' ) '] reboot' | tee -a /var/log/$( basename $0 ).log
  /sbin/init 6
fi