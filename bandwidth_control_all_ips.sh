#!/bin/bash
# create bandwidth control to all IPs of lan
 
BANDWIDTH_KBIT=27000
BANDWIDTH_INTERFACE=eth0
LAN_IP=$( ip addr show $BANDWIDTH_INTERFACE | grep -Eo '([0-9]{1,3}\.){3}' | sed -n 1p )
 
if [ $# -eq 0 ]; then
  echo -e "\n\e[31;1m[ ERRO ]\e[m Use: $( basename $0 ) \e[32;1mstart\e[m | \e[33;1mstop\e[m\n"; exit
fi
 
function clear_all_tc_rules() {
 
  tc qdisc del dev $BANDWIDTH_INTERFACE root
}
 
function create_tc_rules() {
 
  tc qdisc add dev $BANDWIDTH_INTERFACE root handle 1: htb
 
  for i in {1..254}; do
    tc class add dev $BANDWIDTH_INTERFACE classid 1:${i} htb rate ${BANDWIDTH_KBIT}kbit
    tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip dst $LAN_IP${i} flowid 1:${i}
  done
}
 
if [ "$1" == 'start' ]; then
  echo -e "\e[32;1mcreating tc rules...\e[m"
  clear_all_tc_rules
  create_tc_rules
elif [ "$1" == 'stop' ]; then
  echo -e "\e[33;1mcleaning up tc rules...\e[m"
  clear_all_tc_rules
fi