#!/bin/bash
# create bandwidth control for some IPs with slow urls
 
BANDWIDTH_KBIT=27000
BANDWIDTH_INTERFACE=eth1
FINAL_IPs_WITH_BANDWIDTH_CONTROL=( 7 8 50 111 )
EXCEPTION_IPS_TO_SLOW_URL=( 101 )
LAN_IP=$( ip addr show $BANDWIDTH_INTERFACE | grep -Eo '([0-9]{1,3}\.){3}' | sed -n 1p )
 
if [ $# -eq 0 ]; then
  echo -e "\n\e[31;1m[ ERRO ]\e[m Use: $( basename $0 ) \e[32;1mstart\e[m | \e[33;1mstop\e[m\n"; exit
fi
 
function clear_all_tc_rules() {
 
  tc qdisc del dev $BANDWIDTH_INTERFACE root
  tc qdisc add dev $BANDWIDTH_INTERFACE root handle 1: htb
}
 
# Create rules to IPs that dont have urls slow
function create_exception_slow_url_rules() {
 
  for i in ${EXCEPTION_IPS_TO_SLOW_URL[*]}; do
    tc class add dev $BANDWIDTH_INTERFACE classid 1:${i} htb rate ${BANDWIDTH_KBIT}kbit
    tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip dst $LAN_IP${i} flowid 1:${i}
  done
}
 
# Create rules to slow access of all internal ips to urls(IPs) below
function create_tc_slow_url_rules() {
 
  tc class add dev $BANDWIDTH_INTERFACE classid 1:500 htb rate 15kbit
 
  # rules to block facebook.com - ips get with nslookup
  tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip src 31.13.83.36 flowid 1:500
  tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip src 157.240.20.35 flowid 1:500
  tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip src 185.60.216.35 flowid 1:500
}
 
function create_tc_bandwidth_rules() {
 
  for i in ${FINAL_IPs_WITH_BANDWIDTH_CONTROL[*]}; do
    tc class add dev $BANDWIDTH_INTERFACE classid 1:${i} htb rate ${BANDWIDTH_KBIT}kbit
    tc filter add dev $BANDWIDTH_INTERFACE parent 1: protocol ip prio 1 u32 match ip dst $LAN_IP${i} flowid 1:${i}
  done
}
 
 
if [ "$1" == 'start' ]; then
  echo -e "\e[33;1mcleaning up tc rules...\e[m"
  clear_all_tc_rules
  echo -e "\e[32;1mcreating exception to slow tc url rules...\e[m"
  create_exception_slow_url_rules
  echo -e "\e[32;1mcreating slow url rules...\e[m"
  create_tc_slow_url_rules
  echo -e "\e[32;1mcreating tc rules...\e[m"
  create_tc_bandwidth_rules
elif [ "$1" == 'stop' ]; then
  echo -e "\e[33;1mcleaning up tc rules...\e[m"
  clear_all_tc_rules
fi