#!/bin/bash
# analyzes apache log and blocking ip than access url 3 times in 5 minutes
# block only access to url not to all urls
 
# you can use | to separate multiple strings to block
URL_STRING_TO_BLOCK="insertPartOfUrl1Here|insertPartOfUrl2Here"
 
CURRENT_TIME=$( date '+%H:%M' )
 
# make array with 5 past minutes using | separator. used as a separator field in the egrep regex 
for i in {1..5};do
  CURRENT_TIME=${CURRENT_TIME}\|$(date -d "-$i min" '+%H:%M:') 
done
 
# list of ips with hits to the same url
IPs_LIST=$( egrep "$(date '+%d\/%b\/%Y:')($CURRENT_TIME).*($URL_STRING_TO_BLOCK)" /var/log/httpd/extranet-access_log )
 
# group ips by number of hits to the same url
GROUP_BY_IPs=$( echo "$IPs_LIST" | cut -d' ' -f1 | sort | uniq -c )
 
# create iptables rule to block each ip that access url 3 times in N minutes than not starts with 192.168.0 range
for i in $( echo "$GROUP_BY_IPs" | awk '$1 >= 3 && $2 !~ "^192.168.0" {print $2}' );do
  /sbin/iptables -nvL INPUT | grep " $i .*many" > /dev/null
  if [ $? -ne 0 ]; then
    for j in $( echo $URL_STRING_TO_BLOCK | sed 's/|/\n/g' ); do
      echo $( date '+%d/%m/%Y %R' ) "blocking ip: $i than access part of URL: $j" | tee -a /var/log/$( basename $0 ).log
      /sbin/iptables -A INPUT -s $i -p tcp -m tcp --dport 80 -m string --string "$j" --algo bm --to 65535 -m comment --comment "block IP than access this URL many times" -j DROP
    done
  fi
done